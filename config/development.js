
module.exports = {
  caches: {
    output: {
      type: 'memory',
      options: {
        limit: 100,
      },
    },
  },
  logger: {
    level: 'debug',
    pretty: true,
  },
};

