---
id: version-2.3.5-progressive
title: Progressive images
sidebar_label: Progressive
original_id: progressive
---

Caravaggio returns progressive images by default, unless you configured it [differently](configuration.md#progressive-images).

If you want to force the progressive output you can specify a prameter like so

**Force progressive**    
<pre><code class="hljs css html" data-preview>https://caravaggio.host/<strong>progressive_true</strong>/https://goo.gl/EXv4MP</code></pre>

**Force non progressive**     
<pre><code class="hljs css html" data-preview>https://caravaggio.host/<strong>progressive_false</strong>/https://goo.gl/EXv4MP</code></pre>
