---
id: version-2.3.5-rotate
title: Rotate
sidebar_label: Rotate
original_id: rotate
---

This paramter let you rotate the image.    
Any angle multiple of `90°` is accepted.

<pre><code class="hljs css html" data-preview>https://caravaggio.host/<strong>rotate_270</strong>/https://goo.gl/EXv4MP</code></pre>

**Original**     
![Two girls](assets/example/girls_small.jpeg)

**Result**     
![Two girls](assets/example/rotate.jpeg)
