---
id: version-2.3.5-flip
title: Flip
sidebar_label: Flip
original_id: flip
---

Fli the image horizontally or vertically. The accepted values are
`flip_x` and `flip_y`



**Original**     
![Image of two girls](assets/example/girls_small.jpeg)

**Flip x**     
![Image of two girls flipped horizontally](assets/example/flipx.jpeg)
<pre><code class="hljs css html" data-preview>https://caravaggio.host/<strong>flip_x</strong>/https://goo.gl/EXv4MP</code></pre>

**Flip y**     
![Image of two girls flipped vertically](assets/example/flipy.jpeg)
<pre><code class="hljs css html" data-preview>https://caravaggio.host/<strong>flip_y</strong>/https://goo.gl/EXv4MP</code></pre>
