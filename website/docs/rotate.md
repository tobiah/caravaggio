---
id: rotate
title: Rotate
sidebar_label: Rotate
---

This paramter let you rotate the image.    
Any angle is accepted.

<pre><code class="hljs css html" data-preview>https://caravaggio.host/<strong>rotate_270</strong>/https://goo.gl/EXv4MP</code></pre>

**Original**     
![Two girls](assets/example/girls_small.jpeg)

**Result**     
![Two girls rotated](assets/example/rotate.jpeg)

An optional background [color](resize#colors) can be set:

<pre><code class="hljs css html" data-preview>https://caravaggio.host/<strong>rotate_12_ff0000</strong>/https://goo.gl/EXv4MP</code></pre>

**Result**     
![Two girls rotated with a red background color](assets/example/rotate-color.jpeg)

