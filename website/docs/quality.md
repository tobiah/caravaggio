---
id: quality
title: Quality
sidebar_label: Quality
---

This parameter define the quality of the output image. It's a value between `0` and `100`.

<pre><code class="hljs css html" data-preview>https://caravaggio.host/<strong>q_10</strong>/https://goo.gl/EXv4MP</code></pre>

**Original**     
![Image of two girls](assets/example/girls_small.jpeg)

**Result**     
![Image of two girls at low quality](assets/example/q10.jpeg)
