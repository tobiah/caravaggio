#!/usr/bin/env node

const os = require('os');
const path = require('path');
const yargs = require('yargs');

['SIGINT', 'SIGTERM'].forEach((signal) => {
  process.on(signal, () => process.exit(0));
});


const options = yargs // eslint-disable-line no-unused-expressions
  .option('port', {
    alias: 'p',
    default: 8565,
    describe: 'the server will listen to this port',
    type: 'number',
  })

  .option('cache', {
    default: 'memory',
    describe: 'Set the cache system',
    type: 'string',
    choices: ['memory', 'file', 'none'],
    group: 'cache',
  })
  .option('cache-filepath', {
    describe: '[when --cache=file] The file path for the file cache',
    type: 'string',
    default: os.tmpdir(),
    group: 'cache',
  })
  .option('cache-limit', {
    default: 100,
    describe: '[when --cache=memory] The maximum amount of memory to use in MB',
    type: 'number',
    group: 'cache',
  })

  .option('inputcache', {
    default: 'none',
    describe: 'Set the input cache system. This cache save the original images to avoid re-download',
    type: 'string',
    choices: ['memory', 'file', 'none'],
    group: 'input cache',
  })
  .option('inputcache-filepath', {
    describe: '[when --cache=file] The file path for the file cache',
    type: 'string',
    default: os.tmpdir(),
    group: 'input cache',
  })
  .option('inputcache-limit', {
    default: 100,
    describe: '[when --cache=memory] The maximum amount of memory to use in MB',
    type: 'number',
    group: 'input cache',
  })

  .option('whitelist', {
    describe: 'Restrict the images to a list of domains',
    type: 'array',
    group: 'security',
  })

  .option('verbose', {
    alias: 'v',
    describe: 'Increase the verbosity of the application',
    type: 'boolean',
    // conflicts: 'quiet',
    group: 'log',
  })
  .option('quiet', {
    alias: 'q',
    describe: 'Do not output anything',
    type: 'boolean',
    // conflicts: 'verbose',
    group: 'log',
  })
  .option('json', {
    describe: 'Output logs in json format.',
    type: 'boolean',
    default: false,
    group: 'log',
  })
  // .conflicts('verbose', 'quiet')
  .options('guess-type-by-extension', {
    describe: 'use the file extension to guess the file type instead of reading metadata (when possible)',
    type: 'boolean',
    default: false,
    group: 'image manipulation',
  })
  .options('progressive', {
    describe: 'the output images are progressive by default (when applicable)',
    type: 'boolean',
    default: true,
    group: 'image manipulation',
  })
  .options('errors', {
    describe: 'set the error output format',
    type: 'string',
    choices: ['plain', 'html', 'json'],
    default: 'json',
    group: 'misc',
  })
  .options('compress', {
    describe: 'compress the response through gzip/deflate if the browser ask for it',
    type: 'boolean',
    default: false,
    group: 'misc',
  })
  .argv;

const defaultTransformations = [];
if (options.progressive) {
  defaultTransformations.push(['progressive', 'true']);
}

let cacheOptions;
switch (options.cache) {
  case 'file':
    cacheOptions = {
      basePath: options['cache-filepath'],
    };
    break;
  case 'memory':
    cacheOptions = {
      limit: options['cache-limit'],
    };
    break;
  default:
    cacheOptions = {};
    break;
}

let inputCacheOptions;
switch (options.inputcache) {
  case 'file':
    inputCacheOptions = {
      basePath: options['inputcache-filepath'],
    };
    break;
  case 'memory':
    inputCacheOptions = {
      limit: options['inputcache-limit'],
    };
    break;
  default:
    inputCacheOptions = {};
    break;
}

const configuration = {
  port: options.port,
  caches: {
    output: {
      type: options.cache,
      options: cacheOptions,
    },
    input: {
      type: options.inputcache,
      options: inputCacheOptions,
    },
  },
  logger: {
    pretty: !options.json,
    level: options.verbose
      ? 'debug'
      : options.quiet
        ? 'silent'
        : 'info',
  },
  whitelist: options.whitelist || false,
  guessTypeByExtension: options['guess-type-by-extension'],
  defaultTransformations,
  errors: options.errors,
  compress: options.compress,
};

process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', '/config');
process.env.NODE_CONFIG = JSON.stringify(configuration);
process.env.NODE_ENV = process.env.NODE_ENV || 'production';

require('../index');

